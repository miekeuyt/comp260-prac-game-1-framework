﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour {
	public Vector2 move;
	public Vector2 velocity;
	public float maxSpeed = 5.0f;

	// Use this for initialization
	void Start () {
		
	}
	
	void Update() {
		Vector2 direction;
		Vector2 P1direction; 
		Vector2 P2direction;
		if (gameObject.name == "Player 1") {
			P1direction.x = Input.GetAxis ("Horizontal");
			P1direction.y = Input.GetAxis ("Vertical");
			Vector2 P1velocity = P1direction * maxSpeed;
			// move the object
			transform.Translate (P1velocity * Time.deltaTime);
		}
		if (gameObject.name == "Player 2") {
			P2direction.x = Input.GetAxis ("Horizontal2");
			P2direction.y = Input.GetAxis ("Vertical2");
			Vector2 P2velocity = P2direction * maxSpeed;
			// move the object
			transform.Translate (P2velocity * Time.deltaTime);
		}

	}
}
