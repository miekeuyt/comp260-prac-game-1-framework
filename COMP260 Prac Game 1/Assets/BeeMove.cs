﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeMove : MonoBehaviour {

	public float speed = 4.0f;        // metres per second
	public float turnSpeed = 180.0f;  // degrees per second
	public Transform player1;
	public Transform player2;
	public Transform randomTarget;
	public Transform target;
	public Vector2 direction;
	public Vector2 heading = Vector3.right; 


	void Start(){ 
		target = randomTarget;
		detectPlayer ();
	}

	void Update() {
		detectPlayer ();

		direction = target.position - transform.position;

		// calculate how much to turn per frame
		float angle = turnSpeed * Time.deltaTime;

		// turn left or right
		if (direction.IsOnLeft(heading)) {
			// target on left, rotate anticlockwise
			heading = heading.Rotate(angle);
		}
		else {
			// target on right, rotate clockwise
			heading = heading.Rotate(-angle);
		}

		transform.Translate(heading * speed * Time.deltaTime);
	}

	void OnDrawGizmos() {
		// draw heading vector in red
		Gizmos.color = Color.red;
		Gizmos.DrawRay(transform.position, heading);

		// draw target vector in yellow
		Gizmos.color = Color.yellow;
		Vector2 direction = target.position - transform.position;
		Gizmos.DrawRay(transform.position, direction);
	}

	Transform detectPlayer(){
		// get the vector from the bee to the players
		Vector2 P1Direction = player1.position - transform.position;
		Vector2 P2Direction = player2.position - transform.position;

		// Check which player is closer, and choose target
		if ((P1Direction.magnitude < P2Direction.magnitude)) {
			target = player1;

		} else  {
			target = player2;
		}
		return target;
	}

}
